importPackage(Packages.org.csstudio.opibuilder.scriptUtil); 
var pv0 = PVUtil.getDouble(pvs[0]);
if(pv0==1){
    // disable when ch is on
    widget.setPropertyValue("enabled",false);
    // grey background
    widget.setPropertyValue("background_color",ColorFontUtil.getColorFromRGB(191,191,191));
}
else{
    // enable when ch is off
    widget.setPropertyValue("enabled",true);
    // white background
    widget.setPropertyValue("background_color",ColorFontUtil.getColorFromRGB(255,255,255));
}

